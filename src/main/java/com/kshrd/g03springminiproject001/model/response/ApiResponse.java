package com.kshrd.g03springminiproject001.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse<T> {
    private T payload;
    private LocalDateTime date;
    boolean success;

}
