package com.kshrd.g03springminiproject001.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Auth {
    private Integer id;
    private String email;
    private String password;
    private String role;
}
