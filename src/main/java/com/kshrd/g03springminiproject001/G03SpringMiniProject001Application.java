package com.kshrd.g03springminiproject001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class G03SpringMiniProject001Application {

    public static void main(String[] args) {
        SpringApplication.run(G03SpringMiniProject001Application.class, args);
    }

}
