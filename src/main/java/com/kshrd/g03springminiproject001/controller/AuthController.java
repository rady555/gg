package com.kshrd.g03springminiproject001.controller;

import com.kshrd.g03springminiproject001.model.entity.Auth;
import com.kshrd.g03springminiproject001.model.request.AuthRequest;
import com.kshrd.g03springminiproject001.model.response.ApiResponse;
import com.kshrd.g03springminiproject001.service.AuthService;
import jakarta.annotation.Resource;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1")
public class AuthController {
    public final AuthService authService;
    @PostMapping("/register")
    public ResponseEntity<?> registerAuthor(@RequestParam AuthRequest authRequest){
        ApiResponse<Auth> respose = new ApiResponse<>(){

        }
    }
}
